#!/usr/bin/python2.7
import os
import json
import ast
from sklearn.cluster import MeanShift, estimate_bandwidth, KMeans
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import Imputer

## change all the variables
## flattening the input dictionary
def create_input(inp):
    EMAIL = inp[0]
    DATA = inp[1:]

    t2 = []
    for i in DATA:
        t3 = {}
        pre = ""
        for j in i:
            pre = j
            if isinstance(i[j], list):
                for k in i[j]:
                    if isinstance(k, dict):
                        for x in k:
                            if isinstance(k[x], int):
                                k[x] = str(k[x])
                            pre1 = pre + "_" + x
                            if t3.setdefault(pre1, k[x]) != None and k[x] != None:    ## assuming that k[x] is a string
                                t3[pre1] = t3[pre1] + " " + k[x]
                    else:
                        if isinstance(k, int):
                            k = str(x)
                        if t3.setdefault(pre, k) != None:
                            t3[pre] = t3[pre] + " " + k
            elif isinstance(i[j], dict):
                for k in i[j]:
                    pre1 = pre + "_" + k
                    if isinstance(i[j][k], int):
                        i[j][k] = str(i[j][k])
                    if t3.setdefault(pre1, i[j][k]) != None:
                        t3[pre1] = t3[pre1] + " " + i[j][k]
            else:
                if isinstance(i[j], int):
                    i[j] = str(i[j])
                t3[pre] = i[j]
        t2.append(t3)

    q = []
    [q.extend(i.keys())    for i in t2    ]
    q = list(set(q))

    finalSet = []
    for i in t2:
        t = {}
        tmp = list(set(q) - set(i.keys()))
        for j in tmp:
            t[j] = ""
        t.update(i)
        finalSet.append(t)

    return finalSet

def clustering_users(inp):
    inp = inp[1:]
    dv = DictVectorizer()
    X = dv.fit_transform(inp).toarray()

    eb = estimate_bandwidth(X)
    if(eb == 0):
        imp = Imputer()
        X = imp.fit_transform(X)

    eb = estimate_bandwidth(X)
    ms = MeanShift(eb)

    print eb
    print ms.fit_predict(X)
