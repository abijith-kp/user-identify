'''
to test the main function in new_reduced_set_method.py
'''

from new_reduced_set_method import cluster_user_profile
import json
import os
def create_input(path):
    '''
    Create input for testing
    '''

    json_list = []
    for file_name in os.listdir(path):
        n_tmp = file_name.split(".")
        if n_tmp[0] != "" and n_tmp[-1] == "json":
            l_tmp = json.loads(open(path + file_name, "r").read())
            json_list.append(l_tmp)

    return json_list

def test():
    file_list = ["../../Results/all_json_files/"]
    inp = create_input(file_list[0])
    mail = "alexmathew@gmail.com"
    print cluster_user_profile(mail, inp)

if __name__ == "__main__":
    test()
