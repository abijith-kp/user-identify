import re
import jellyfish

'''
Key words to identify each profile dictionaries from their list of dictionary keys
'''
identity_key = {"latest_posts": "aboutme",
                "featured": "angellist",
                "languages": "linked_in",
                "circles": "gplus",
                "tweet": "twitter",
                "avatar": "gravatar",
                "pinboard": "pintrest",
                "upload": "slideshare",
                "checkin": "foursquare",
                "repo": "github"}

'''
Maps the original dictionary keys to the new ones
'''
key_dict = {"works_for": "organization",
#            "profile_name": "profile_name",
            "location": "location",
            "full_name": "full_name",
            "hometown": "hometown",
            "url": "website",
            "basic_info": "basic_info",
            "bio": "description",
            "headline": "description",
            "languages": "languages",
            "description": "description",
            "tags": "description",
            "skills": "skills",
            "current_location": "current_location",
            "previous_location": "previous_location",
            "education": "education",
            "emails": "emails",
#            "social_profiles": "social_profiles",
#            "other_profiles": "social_profiles",
            "websites": "website",
            "site_url": "website"}

def identify_link(link):
    '''
    Input  : link - an url
    Output : tuple containing the website canonical name and the username
    '''
    identify_link_re = [("linked_in", re.compile("[\w\.-]+/pub/([\w\.\-_]+)|[\w\.-]+/in/([\w\.\-_]+)")),
                       #("youtube", re.compile("[\w\.-]+/channel/([\w\.\-_]+)|[\w\.-]+/user/([\w\.\-_]+)")),
                        ("youtube", re.compile("[\w\.-]+/user/([\w\.\-_]+)")),
                        ("flickr", re.compile("[\w\.-]+/people/([\w\.\-_]+)")),
                        ("facebook", re.compile("facebook.com/([\w\.\-_]+)")),
                        ("twitter", re.compile("twitter.com/([\w\.\-_]+)")),
                        ("github", re.compile("github.com/([\w\.\-_]+)")),
                        ("quora", re.compile("quora.com/([\w\.\-_]+)")),
                        ("aboutme", re.compile("about.me/([\w\.\-_]+)"))]

    for identify_website, regular_expression in identify_link_re:
        t_tuple_username = regular_expression.findall(link)
        if t_tuple_username != []:
            if t_tuple_username[0] != "":
                if identify_website == "facebook":
                    return (identify_website, str.join("",t_tuple_username[0]).replace(".", ""))
                return (identify_website, str.join("",t_tuple_username[0]))

    return (("", ""))


def extract_new_features(input_profile_dict):
    '''
    Input  : a list of complete profile details in the form of list of dictionaries
    Output : a dictionary with only required and important feaures for grouping. rules can be added later according to
    the need
    '''

    extracted_feature_dict = {}

    for t_profile_dict in input_profile_dict:
        if input_profile_dict[t_profile_dict] in ["null", "", []] and t_profile_dict != "username":
            continue

        elif t_profile_dict == "avatar":
            extracted_feature_dict[t_profile_dict] = input_profile_dict[t_profile_dict]

        elif t_profile_dict == "profile_name":
            extracted_feature_dict["profile_name"] = input_profile_dict[t_profile_dict].lower()

        elif t_profile_dict == "username":
            extracted_feature_dict["github"] = input_profile_dict[t_profile_dict]
            if "popular_repos" in input_profile_dict.keys() and input_profile_dict[t_profile_dict] == "":
                username_dict = input_profile_dict[t_profile_dict]["popular_repos"][0]   ## first dictionary
                if username_dict != []:
                    link = username_dict["repo_link"]
                    link = re.compile(r'github\.com/([\w\._-]+)').findall(link)[0].lower()
                    extracted_feature_dict["github"] = extracted_feature_dict[t_profile_dict] ## new key github for storing username

        elif t_profile_dict == "hometown" or t_profile_dict.find("location") >= 0:
            temp = input_profile_dict[t_profile_dict]
            temp = re.sub("[()0-9,\- ]", " ", temp).lower()
            temp = temp.split(" ")
            temp = [i.strip()    for i in temp    if len(i.strip()) >= 1]
            if extracted_feature_dict.setdefault("location", temp) != None:
                extracted_feature_dict["location"].extend(temp)
            temp = extracted_feature_dict["location"]
            extracted_feature_dict["location"] = list(set(temp))


        elif t_profile_dict in ["url", "site_url", "websites"]:
            temp = input_profile_dict[t_profile_dict]
            if not isinstance(temp, list):
                temp = [temp.lower()]
            else:
                x = []
                for i in temp:
                    x.append(i["link"].lower())
                temp = x
            if extracted_feature_dict.setdefault("website", temp) != None:
                extracted_feature_dict["website"].extend(temp)
            temp = extracted_feature_dict["website"]
            extracted_feature_dict["website"] = list(set(temp))

        elif t_profile_dict == "works_for":
            if extracted_feature_dict.setdefault("organization", [input_profile_dict[t_profile_dict].lower()]) != None:
                extracted_feature_dict["organization"] = [input_profile_dict[t_profile_dict].lower()]
                extracted_feature_dict['organization'] = list(set(extracted_feature_dict['organization']))

        elif t_profile_dict == "featured":
            tmp_dict_list = input_profile_dict[t_profile_dict]
            if isinstance(tmp_dict_list, list) == True:
                for tmp_dict in tmp_dict_list:
                    if extracted_feature_dict.setdefault('organization', [tmp_dict['company'].lower()]) != None:
                        extracted_feature_dict['organization'].append(tmp_dict['company'].lower())
                        extracted_feature_dict['organization'] = list(set(extracted_feature_dict['organization']))

        elif t_profile_dict in ["other_links", "other_profiles"]:
            tmp_dict_list = input_profile_dict[t_profile_dict]
            for tmp_dict in tmp_dict_list:
                if isinstance(tmp_dict, str) == True or isinstance(tmp_dict, unicode) == True:
                    web, name = identify_link(tmp_dict)
                    if web == "":
                        name = tmp_dict["link"]
                        if extracted_feature_dict.setdefault("username", [name]) != None:
                            extracted_feature_dict["username"].append(name)
                    else:
                        if web not in extracted_feature_dict.keys() or extracted_feature_dict[web] in ["", "null", None]:
                            extracted_feature_dict[web] = name
                    continue
                elif "profile" not in tmp_dict.keys():
                    continue
                elif tmp_dict["profile"] == "website-link":
                    temp = [tmp_dict["link"].lower()]
                    if extracted_feature_dict.setdefault("website", temp) != None:
                        extracted_feature_dict["website"].extend(temp)
                    temp = extracted_feature_dict["website"]
                    extracted_feature_dict["website"] = list(set(temp))
                else:
                    web, name = identify_link(tmp_dict["link"])
                    if web == "":
                        name = tmp_dict["link"]
                        if extracted_feature_dict.setdefault("username", [name]) != None:
                            extracted_feature_dict["username"].append(name)
                    else:
                        if web not in extracted_feature_dict.keys() or extracted_feature_dict[web] in ["", "null", None]:
                            extracted_feature_dict[web] = name

        elif t_profile_dict == "emails":
            extracted_feature_dict["email"] = input_profile_dict[t_profile_dict]

#        elif t_profile_dict == "skills":
#            if isinstance(input_profile_dict[t_profile_dict], list):
#                extracted_feature_dict[t_profile_dict] = input_profile_dict[t_profile_dict]
#                extracted_feature_dict[t_profile_dict] = map((lambda x: x.lower().strip()), extracted_feature_dict[t_profile_dict])
#            else:
#                extracted_feature_dict[t_profile_dict] = input_profile_dict[t_profile_dict].split(", ")
#                extracted_feature_dict[t_profile_dict] = map((lambda x: x.lower().strip()), extracted_feature_dict[t_profile_dict])
    if "username" in extracted_feature_dict.keys():
        extracted_feature_dict['username'] = list(set(extracted_feature_dict['username']))
    return extracted_feature_dict


def list_uniq(list1):
    '''
    Input  : list of items
    Output : returns unique items from the given list
    '''
    list2 = list1
    for i in list1:
        while i in list1:
            list1.pop(list1.index(i))
        list1.append(i)

    return list1


def list_intersect(list1, list2):
    '''
    Input  : two list of items
    Output : returns intersection of the items from the given lists
    '''
    intersect_list = []
    for i in list1:
        if i in list2:
            intersect_list.append(i)
    return intersect_list


def list_union(list1, list2):
    '''
    Input  : two list of items
    Output : returns union of the items from the given lists
    '''
    union_list = list1
    for i in list2:
        if i not in union_list:
            union_list.append(i)

    return union_list


def initial_group_users(input_email, input_profile_list):
    '''
    Input  : input_email, input_profile_list
    Output : groups users in a bottom-up approach and returns a list of groups of profiles
    '''
    to_check_keys_list = ["email", "facebook", "github", "twitter", "quora","youtube",
                          "linked_in", "flickr", "aboutme", "website", "profile_name"]

    unified_set = []
    t_process_group = {}
    for check_key in to_check_keys_list:
        t_process_group = {}
        for t_profile_dict in input_profile_list:
            if check_key in t_profile_dict.keys():
                if isinstance(t_profile_dict[check_key], list):
                    for every in t_profile_dict[check_key]:
                        if t_process_group.setdefault(every, [t_profile_dict]) != [t_profile_dict]:
                            t_process_group[every].append(t_profile_dict)
                elif t_process_group.setdefault(t_profile_dict[check_key], [t_profile_dict]) != [t_profile_dict]:
                    t_process_group[t_profile_dict[check_key]].append(t_profile_dict)

        if unified_set == []:
            for tmp in t_process_group:
                unified_set.append(list_uniq(t_process_group[tmp]))
        else:
            t_profile_dict = []
            for tmp in t_process_group:
                t_profile_dict.append(list_uniq(t_process_group[tmp]))

            for j in t_profile_dict:
                flag = 0
                for i in unified_set:
                    if list_intersect(j, i) != []:
                        unified_set[unified_set.index(i)] = list_union(i, j)
                        flag = 1
                        break
                if flag == 0:
                    unified_set.append(j)

    for i in unified_set:
        for j in unified_set:
            if list_intersect(j, i) != [] and i != j:
                unified_set[unified_set.index(i)] = list_union(i, j)
                unified_set.pop(unified_set.index(j))

    return unified_set


def final_email_comparer(input_email, input_profile_list):
    '''
    Critiria for assigning the flags:
        Email correct match     : 0
        Email handle match      : 1
        Probable work match     : 2
        Others                  : 3
    '''
    mail_split = input_email.split("@")
    mail_id = mail_split[0]
    pos_work = mail_split[1].split(".")[0]
    N = len(input_profile_list)
    flag = [3    for i in xrange(N)   ]

    for t_profile_dict in input_profile_list:
        for t_key_dict in t_profile_dict:
            if t_key_dict in ["location", "profile_id"]:
                continue
            elif t_key_dict == "email":
                for t in t_profile_dict[t_key_dict]:
                    mail_s = t.split("@")
                    mail_s[-1:] = mail_s[-1].split(".")
                    if input_email == t:
                        if flag[input_profile_list.index(t_profile_dict)] > 0:
                            flag[input_profile_list.index(t_profile_dict)] = 0
                    if mail_s[0] == mail_id:
                        if flag[input_profile_list.index(t_profile_dict)] > 1:
                            flag[input_profile_list.index(t_profile_dict)] = 1
                        if mail_s[1] == pos_work:
                            if flag[input_profile_list.index(t_profile_dict)] == 1:
                                flag[input_profile_list.index(t_profile_dict)] = 0.5
                            else:
                                flag[input_profile_list.index(t_profile_dict)] = 2
            elif t_key_dict == "organization":
                for t in t_profile_dict[t_key_dict]:
                    if re.sub("[\-_\. ]", "", t).find(pos_work) >= 0 or pos_work.find(re.sub("[\-_\. ]", "", t)) >= 0:
                        if flag[input_profile_list.index(t_profile_dict)] == 1:
                            flag[input_profile_list.index(t_profile_dict)] = 0.5
                        else:
                            flag[input_profile_list.index(t_profile_dict)] = 2
            else:
                for t in t_profile_dict[t_key_dict]:
                    if mail_id.find(re.sub("[\-_\. ]", "", t)) >= 0 or re.sub("[\-_\. ]", "", t).find(mail_id) >= 0:
                        if flag[input_profile_list.index(t_profile_dict)] > 0:
                            flag[input_profile_list.index(t_profile_dict)] = 1
    return flag


def identify_representatives(input_profile_list):
    '''
    Input  : list list of dictionaries
    Output : list of dictionaries after all the individual list of dictionaries are mearged into a single one
    '''

    representative_list = []

    for group in input_profile_list:
        tmp_repr = {}
        for member in group:
            if tmp_repr == {}:
                for inp in member:
                    if isinstance(member[inp], list):
                        tmp_repr[inp] = member[inp]
                    else:
                        tmp_repr[inp] = [member[inp]]
            else:
                for inp in member:
                    if inp in tmp_repr:
                        if isinstance(member[inp], list):
                            tmp_repr[inp] = list(set(tmp_repr[inp]) | set(member[inp]))
                        else:
                            tmp_repr[inp] = list(set(tmp_repr[inp]) | set([member[inp]]))
                    elif inp not in tmp_repr:
                        if isinstance(member[inp], list):
                            tmp_repr[inp] = member[inp]
                        else:
                            tmp_repr[inp] = [member[inp]]
        representative_list.append(tmp_repr)
    return representative_list



def identify_site_processing(profile_dict):
    '''
    Input  : A dictionary representaing a profile
    Output : Groups all the attributes with respect to the website identified
    '''

    key_join_string = " ".join(profile_dict.keys())
    site_identity = ""
    result_dictionary = {}
    t_dict = {}

    for t_key in identity_key:
        if t_key in key_join_string:
            site_identity = identity_key[t_key]
            break

    for t_key in profile_dict:
        if t_key == "profile_id" or profile_dict[t_key] in ["", "null", []]:
            continue
        if t_key in key_dict.keys():
            if isinstance(profile_dict[t_key], list):
                if result_dictionary.has_key(key_dict[t_key]):
                    result_dictionary[key_dict[t_key]].extend(profile_dict[t_key])
                else:
                    result_dictionary[key_dict[t_key]] = profile_dict[t_key]
            else:
                if result_dictionary.has_key(key_dict[t_key]):
                    result_dictionary[key_dict[t_key]].append(profile_dict[t_key])
                else:
                    result_dictionary[key_dict[t_key]] = [profile_dict[t_key]]
        else:
            t_dict[t_key] = profile_dict[t_key]

    result_dictionary[site_identity] = t_dict

    return result_dictionary


def matcher(string_list):
    '''
    Input  : List of strings
    Output : Set of strings. All similar kinds of strings are merged togather
             and the most
    '''
    string_list = list(set(string_list))
    string_list.sort()

    flag = 0

    for i in string_list:
        flag = 0
        f = string_list.index(i) + 1
        for j in string_list:
            if jellyfish.jaro_winkler(i, j) >= 0.90 and string_list.index(i) != string_list.index(j):
                flag = 1
                break
        if flag == 1:
            string_list.pop(string_list.index(i))

    return string_list

def merger_original_profiles(profiles_list, representative):
    '''
    Merge all the profiles in the given group. Each wesites is idividually identified for better representation.
    '''

    result_dictionary = {}

    for t_dict in profiles_list:
        t_dict_p = identify_site_processing(t_dict)

        if result_dictionary == {}:
            result_dictionary = t_dict_p
        else:
            for t_key in t_dict_p:
                if t_key in result_dictionary:
                    result_dictionary[t_key].extend(t_dict_p[t_key])
                else:
                    result_dictionary[t_key] = t_dict_p[t_key]

    for t_key in result_dictionary:
        if t_key == "website" and t_key in representative:
            result_dictionary[t_key] = matcher(representative[t_key])
            continue
        elif t_key == "description":
            result_dictionary[t_key] = matcher(result_dictionary[t_key])
            continue
        elif isinstance(result_dictionary[t_key], list):
            result_dictionary[t_key] = list_uniq(result_dictionary[t_key])

    if "profile_name" in representative:
        result_dictionary['name'] = representative['profile_name'].pop()

    return result_dictionary


def print_original_profile(processed_profile_list, input_profile_list):
    '''
    Reverse function to map from processed set to the original profiles
    '''

    t_print_list = []

    for t_profile_dict in processed_profile_list:
        t_print_list.append(input_profile_list[t_profile_dict["profile_id"]])

    return t_print_list


def cluster_user_profile(input_email, input_profile_list):
    '''
    Input  : input_email        : email address with which each profile is to be compared with
             input_profile_list : list of dictionaries which represents a profile
    Output : groups of profiles which matches with the given email address
    '''

    processed_profile_list = []
    profile_id = 0
    final_output = []

    for each_profile in input_profile_list:
        processed_profile_list.append(extract_new_features(each_profile))
        processed_profile_list[-1]["profile_id"] = profile_id
        input_profile_list[profile_id]["profile_id"] = profile_id
        profile_id = profile_id + 1

    processed_group_list = initial_group_users(input_email, processed_profile_list)
    representative_list = identify_representatives(processed_group_list)
    flag = final_email_comparer(input_email, representative_list)
    t_flag = flag[:]
    t_flag.sort()
    minimum_flag = t_flag[0]
    count = 0
    if minimum_flag < 2:
        for i in flag:
            if i == minimum_flag:
                t = print_original_profile(processed_group_list[count], input_profile_list)
                t = merger_original_profiles(t, representative_list[count])
                final_output.append(t)
            count = count + 1

    return final_output
