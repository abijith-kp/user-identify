import pprint
import try_new
import text_method
import re
import difflib
import ast
import nltk
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer


##for printing purpose: label creation
#def label_input(inp_list, to_do):
#    if to_do == 0:


## Input : list of dictionaries after processing from the json file
## Output: reduce the larger set into a smaller one based on the rules below given
def get_new_list(inp):
    new_list = {}

    for val in inp:
        if inp[val] in ["null", "", []]:
            continue
        if val in ["profile_name", "username", "full_name"]:
            if val == "full_name":
                new_list["profile_name"] = inp[val]
            elif isinstance(inp[val], unicode) or isinstance(inp[val], str):
                new_list[val] = inp[val].lower()

            if val == "username" and inp[val] == "":
                if "popular_repos" in inp.keys():
                    username_dict = inp[val]["popular_repos"][0]   ## first dictionary
                    if username_dict != []:
                        link = username_dict["repo_link"]
                        new_list[val] = re.compile(r'github\.com/([\w\._-]+)').findall(link)[0].lower()
                        new_list["github"] = new_list[val].lower()         ## new key github for storing username

        elif val == "hometown":
            if new_list.setdefault("location", inp["hometown"].split(",")) != None:
                new_list["location"].extend(inp["hometown"].split(","))

        elif val.find("location") >= 0:
            new_list[val] = inp[val].split(",")

        elif val in ["url", "site_url", "website"]:
            new_list["website"] = inp[val].lower()

        elif val == "works_for":
            new_list["organization"] = [inp[val].lower()]
        elif val == "featured":
            tmp_dict_list = inp[val]
            if isinstance(tmp_dict_list, list) == True:
                for tmp_dict in tmp_dict_list:
                    if new_list.setdefault('organization', [tmp_dict['company'].lower()]) != None and new_list.setdefault('role', [tmp_dict['role'].lower()]) != None:
                        new_list['organization'].append(tmp_dict['company'].lower())
                        new_list['role'].append(tmp_dict['role'].lower())
                        new_list['organization'] = list(set(new_list['organization']))
                        new_list['role'] = list(set(new_list['role']))

        elif val in ["other_links", "other_profiles"]:
            tmp_dict_list = inp[val]
            for tmp_dict in tmp_dict_list:
                if isinstance(tmp_dict, dict) == False:
                    continue
                if "profile" not in tmp_dict.keys():
                    continue
                if tmp_dict["profile"] == "website-link":
                    new_list["website"] = tmp_dict["link"].lower()
                elif tmp_dict["profile"] == "Blog":
                    new_list["blog"] = tmp_dict["link"].lower()
                else:
                    name = re.findall("[\w\.-]+/reader/shared/([\w\.\-_]+)|[\w\.-]+/people/([\w\.\-_]+)|[\w\.-]+/user/([\w\.\-_]+)|[\w\.-]+/pub/([\w\.\-_]+)|[\w\.-]+/in/([\w\.\-_]+)|[\w\.-]+/([\w\.\-_]+)", tmp_dict["link"])
                    if name == []:
                        name = tmp_dict["link"]
                    else:
                        name = str.join("", name[0])
                    k = tmp_dict["profile"].split("-")[0].lower()
                    if k not in new_list.keys() or new_list[k] == "":
                        new_list[tmp_dict["profile"].split("-")[0].lower()] = name.lower()

        elif val == "basic_info":
            new_list.update(inp[val])

        elif val == "emails":
            new_list[val] = inp[val]

        #elif val in ['description', 'bio', 'skills', 'tags', 'industry']:
            #print val, inp[val], "\n"
##        elif val == "pinboards":
##            tmp_dict_list = inp[val]
##            for tmp_dict in tmp_dict_list:
##                name = re.findall("pinterest.com/([\w\.\-_]+)", tmp_dict["link"])[0]
##                new_list["pinterest"] = name
    return new_list

## Input : abbreviated text and the larger one
## Output: True if it is a subsequence(ie. abbreviated text) else False
def is_abbrev(abbrev, text):
    pattern = ".*".join(abbrev.lower())
    return re.match("^" + pattern, text.lower()) is not None

##
##
def check_user_names(email, inp_list):
    conf_90 = []
    others = []
    match = []

    mail_id = email.split("@")[0]
    mail_id = re.sub("[\.\-_ ]", "", mail_id)
    for inp in inp_list:
        val_ratio = 0
        flag = 0
        name = ""
        for k in inp:
            if k in ["username", "linked_in", "facebook", "github", "youtube", "aboutme", "twitter"]:
                name = re.sub("[\.\-_ ]", "", inp[k])
                val_ratio = difflib.SequenceMatcher(a=name, b=mail_id).ratio()
            elif k == "profile_name":
                name = re.sub("[\.\-_ ]", "", inp[k])
                val_ratio = difflib.SequenceMatcher(a=name, b=mail_id).ratio()

            if val_ratio >= 0.90:
                conf_90.append(inp)
                flag = 1
                break
            if is_abbrev(mail_id, name) and flag != 2:
                match.append(inp)
                flag = 2

        if flag == 0:
            others.append(inp)

    return [conf_90, match, others]

def hash_comparer(inp_list, key_name, split_opt, re_opt):
    hash_list = {}
    flag = 0

    for inp in inp_list:
        test_word_list = inp[key_name]
        for test_word in test_word_list:
            if re_opt == 1:
                test_word = re.sub("[\.\-_]","",test_word)
            if hash_list == {}:
                hash_list[test_word] = [inp]
                continue
            for word in hash_list.keys():
                flag = 0
                for i, j in zip(word.split(split_opt), test_word.split(split_opt)):
                    if difflib.SequenceMatcher(a=i, b=j).ratio() < 0.90:
                        flag = 1
                        break
                if flag == 0:
                    if len(word) >= len(test_word):
                        hash_list[word].append(inp)
                    else:
                        t = hash_list.pop(word)
                        t.append(inp)
                        hash_list[test_word] = t
                    flag = 2
                    break
            if flag == 1:
                hash_list[test_word] = [inp]

    return hash_list

##
##
def check_user_websites(email, inp_list):
    exact = []
    conf_90 = []
    others = []

    main_group = [[], []]
    web = email.split("@")[1]

    for l in inp_list:
        if "website" in l.keys():
            l['website'] = [re.sub("https?://www\.|https?://", "", l['website'])]
            main_group[0].append(l)
        else:
            main_group[1].append(l)

    ## Make adjustment here to on how to compare websites aftr hash_comparer function is called.
    hash_list = hash_comparer(main_group[0], 'website', "/", 0)
    for website in hash_list:
        if website.startswith(web):
            exact.extend(hash_list[website])
        elif difflib.SequenceMatcher(a=website, b=web).ratio() >= 0.90:
            conf_90.extend(hash_list[website])
        else:
            others.extend(hash_list[website])

    others.extend(main_group[1])
    return [exact, conf_90, others]

##
##
#assuming mail to of the form abc@company.something
def check_user_work(email, inp_list):
    conf_90 = []
    match = []
    others = []

    main_group = [[], []]
    hash_list = {}
    prob_work = re.findall("\w+@(\w+)\.\w+", email)[0]

    for i in inp_list:
        if 'organization' in i.keys():
            main_group[0].append(i)
        else:
            main_group[1].append(i)

    hash_list = hash_comparer(main_group[0], 'organization', " ", 1)
    temp = []
    for name in hash_list:
        if difflib.SequenceMatcher(a=prob_work, b=name).ratio() >= 0.90:
            conf_90.extend(hash_list[name])
        elif is_abbrev(prob_work, name) == True:
            match.extend(hash_list[name])
        else:
            others.extend(hash_list[name])

    others.extend(main_group[1])

    return [conf_90, match, others]

##
##
def check_user_location(inp_list):
    for i in inp_list:
        for j in i:
            if j.find('location') >= 0:
                print i[j]
        print "==" * 10

    print len(inp_list)

##
##
def check_user_email(email, inp_list):
    exact = []
    conf_90 = []
    others = []
    mail_id = email.split("@")[0]

    main_group = [[], []]
    for i in inp_list:
        if "emails" in i.keys():
            main_group[0].append(i)
        else:
            main_group[1].append(i)

    for inp in main_group[0]:
        for mail in inp['emails']:
            if mail == email:
                exact.append(inp)
                main_group[0].pop(main_group[0].index(inp))
                break

    hash_list = hash_comparer(main_group[0], "emails", "@", 0)
    for mail in hash_list:
        if difflib.SequenceMatcher(a=mail_id, b=mail.split("@")[0]).ratio() >= 0.90:
            conf_90.extend(hash_list[mail])
        else:
            others.extend(hash_list[mail])

    others.extend(main_group[1])
    return [exact, conf_90, others]

def set_calc(list1, list2):
    set1 = []
    set2 = []
    impotance_list = []
    order = [(0, 0), (1, 0), (0, 1), (1, 1), (2, 0), (0, 2), (1, 2), (2, 1), (2, 2)]

    for i in list1:
        set1.append(set(map(str, i)))
    for i in list2:
        set2.append(set(map(str, i)))

    for i, j in order:
        temp_list = set1[i] & set2[j]
        if temp_list != set():
            impotance_list.append(list(temp_list))

    tmp = []
    for i in impotance_list:
        tmp.append(map(ast.literal_eval, i))

    impotance_list = tmp
    return impotance_list


def cluster_method_2(inp_list):
    dictionary = {}
    new_list = []

    for i in inp_list:
        string = ""
        for j in i.values():
            if isinstance(j, list):
                string = string + " " + str.join(" ", j)
            else:
                string = string + " " + j
        string = re.sub("[\-_\.]", " ", string).lower()
        new_list.append(string)

    count = 0
    for i in inp_list:
        for j in i.values():
            if isinstance(j, list):
                for k in j:
                    t = re.sub("[\-_\.]", " ", k).lower()
                    t = t.split(" ")
                    t_list = []
                    for w in xrange(1, len(t)):
                        t_list.extend([str.join(" ", i)    for i in list(nltk.ngrams(t, w))    ])
                    for w in t_list:
                        if w not in dictionary.keys():
                            dictionary[w] = count
                            count = count + 1
            else:
                t = re.sub("[\-_\.]", " ", j).lower()
                t = t.split(" ")
                t_list = []
                for k in xrange(1, len(t)):
                    t_list.extend([str.join(" ", i)    for i in list(nltk.ngrams(t, k))    ])
                for k in t_list:
                    if k not in dictionary.keys():
                        dictionary[k] = count
                        count = count + 1

#    cv = CountVectorizer(vocabulary=dictionary)
#    X = cv.fit_transform(new_list).toarray()
    tv = TfidfVectorizer(vocabulary=dictionary)
    X = tv.fit_transform(new_list).toarray()

    ms = MeanShift(estimate_bandwidth(X))
    label = ms.fit_predict(X)

    N = ms.cluster_centers_.shape[0]
    group = []
    for i in xrange(N):
        group.append([])

    c = 0
    for i in label:
        group[i].append(inp_list[c])
        c = c + 1

    pprint.pprint(group)
    pprint.pprint(dictionary)
    pprint.pprint(new_list)

##
##
def cluster_inp(email, inp_list):
    cluster_methods = [("email", check_user_email), ("names", check_user_names), ("work", check_user_work), ("website",
        check_user_websites)]

    new_list = []
    for inp in inp_list:
        new_list.append(get_new_list(inp))

    mail = check_user_email(email, new_list)
    name = check_user_names(email, new_list)
    imp_list = set_calc(mail, name)

    for method in cluster_methods[2:]:
        tmp = []
        for tmp_imp_list in imp_list:
            tmp.extend(check_user_work(email, tmp_imp_list))
        imp_list = tmp

    pprint.pprint(imp_list)
    c = 0
    for i in imp_list:
        print c, len(i)
        c = c + 1

    #cluster_method_2(new_list)

if __name__ == "__main__":
    inp = []
    for file in try_new.X:
        inp.extend(text_method.create_input("", file)[1:])
    X = ["alexm@profoundis.com"]#, "alexm@gmail.com", "sriniketh@gmail.com", "sachin@gmail.com"]
    #X = ["ls@profoundis.op"]
    for i in X:
        print "-------" * 10
        cluster_inp(i, inp)
