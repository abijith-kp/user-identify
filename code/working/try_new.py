import text_method
import dict_method
import numpy as np
from sklearn.cluster import AffinityPropagation
from sklearn import metrics
import nltk
import random
import difflib

#inp_list is the list that is to be processed. It contains list of folders.
def new_input(inp_list):
    l = [""]
    for i in inp_list:
        l_tmp = text_method.create_input("", i)
        l.extend(l_tmp[1:])
        l[1:] = random.sample(l[1:], len(l)-1)

    l[1:] = random.sample(l[1:], len(l)-1)
    l = dict_method.create_input(l)
    l = random.sample(l, len(l))
    return l

def find_user(email, inp_list):
    #imp = ["email"]
    imp = ['name', 'location', 'skills', 'education', "description", "bio", "email", "website", "experience"]
    l = inp_list
    N = len(l)

    ap = AffinityPropagation(affinity="precomputed")
    attr_list = []
    for i in xrange(N):
        attr_list.append({})

    new_list = []
    count = 1

    cluster_list = []
    cluster_center = []
    imp_cluster = []
    text_list = []
    dict_cluster = {}
    dist = []

    for count in xrange(len(imp)):
        new_list = []
        text_list = []
        k = imp[count]
        for x in zip(l, attr_list):
            i, t = x
            for j in i:
                if j.find(k) >= 0 and i[j] != "":
                    t[j] = i[j]
            new_list.append(t)
        attr_list = new_list
        text_list = [str.join(" ", i.values()).lower()    for i in new_list    ]

        print len(text_list)
        dist = np.zeros((N, N))
        for i in xrange(N):
            for j in xrange(N):
                dist[i][j] = difflib.SequenceMatcher(a=text_list[i], b=text_list[j]).ratio()

        print imp[:count+1]
        lab = ap.fit_predict(dist)
        print lab
        #print "SS = ", metrics.silhouette_score(dist, lab)
        SS = metrics.silhouette_score(dist, lab)

        temp = {}
        for n in xrange(len(ap.cluster_centers_indices_)):
            temp[n] = []

        c = 0
        for n in ap.labels_:
            temp[n].append(c)
            c = c + 1

        print ap.cluster_centers_indices_
        cluster_list.append(temp)
        cluster_center.append(ap.cluster_centers_indices_)

        cc = ap.cluster_centers_indices_
        for n in xrange(len(cc)):
            cluster_points = temp[n]
            cluster_c = cc[n]
            punct_text = nltk.wordpunct_tokenize(text_list[cluster_c])
            print "CP ", len(punct_text)
            for word in punct_text:
                if difflib.SequenceMatcher(a=word, b=email).ratio() > 0.5 or difflib.SequenceMatcher(a=word, b=email.split("@")[0]).ratio() > 0.7:
                    imp_cluster.append(cluster_points)
                    if dict_cluster.setdefault(cluster_c, cluster_points) != None:
                        dict_cluster[cluster_c] = list(set(cluster_points) & set(dict_cluster[cluster_c]))
                    for y in cluster_points:
                        #print "DIST:", dist[cluster_c][y], " ", text_list[y]
                        print "--" * 10
                    break
            print "==" * 10

    print imp_cluster
    print dict_cluster
    for i in dict_cluster:
        print ">> ", i
        print text_list[i]
        for j in dict_cluster[i]:
            print "<< ", j, " ", dist[i][j]
            if dist[i][j] > 0.5:
                print text_list[j]
    set_q = []
    for i in imp_cluster:
        if set_q == []:
            set_q = i
        else:
            set_q = list(set(set_q) & set(i))

    for i in set_q:
        print i, " ", text_list[i]

X = ["../../Results/atm/", "../../Results/sriniketh/", "../../Results/alexmathew/", "../../Results/sachin/", "../../Results/shankarganesh/"]

def run(email):
    inp_list = new_input(X)
    find_user(email, inp_list)

if __name__ == "__main__":
    email = "me@imsach.in"
    run(email)
