import os
import json
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.cluster import MeanShift, estimate_bandwidth

def create_input(email, path):
    json_list = [email]

    for file_name in os.listdir(path):
        n_tmp = file_name.split(".")
        if n_tmp[0] != "" and n_tmp[-1] == "json":
            l_tmp = json.loads(open(path + file_name, "r").read())
            json_list.append(l_tmp)

    return json_list

def clustering_users(inp):
    EMAIL = inp[0]
    DATA = inp[1:]

############################################################################
############# converstion of dictionary into a text ########################
############################################################################

    tmp_data_processed = []
    for tmp_data in DATA:
        t = []
        for k in tmp_data:
            if isinstance(tmp_data[k], list):
                for nk in tmp_data[k]:
                    if isinstance(nk, dict):
                        t.extend(nk.values())
                    else:
                        t.append(nk)
            elif isinstance(tmp_data[k], dict):
                t.extend(tmp_data[k].values())
            else:
                t.append(tmp_data[k])

        tmp_data_processed.append(t)

    data_processed = []
    for tmp_data in tmp_data_processed:
        tmp = []
        for i in tmp_data:                  ### converting int to str and removin None
            if isinstance(i, int):
                tmp.append(str(i))
            elif i != None:
                tmp.append(i)
        data_processed.append(tmp)

    data_processed = [str.join(" ", i)    for i in data_processed    ]
############################################################################

############### fitting and predicting the cluster #########################

    ## tv = TfidfVectorizer(min_df=1, ngram_range=(1,3))
    ## X = tv.fit_transform(data_processed).toarray()
    print data_processed
    cv = CountVectorizer()
    X = cv.fit_transform(data_processed).toarray()
    if estimate_bandwidth(X) == 0:
        print "WARNING: estimate_bandwidth is zero"
        return ""
    ms = MeanShift(estimate_bandwidth(X))
    return ms.fit_predict(X)

if __name__ == "__main__":
    inp = create_input("me@imsach.in", "../../Results/")
    print clustering_users(inp)
