import os
from sklearn.feature_extraction import DictVectorizer
from sklearn.cluster import MeanShift, estimate_bandwidth
import json

def user_identify(inp):
    feature_list = []
    [feature_list.extend(i.keys()) for i in inp[1:]    ]
    feature_list = list(set(feature_list))
    ## print feature_list

    inp_process = []
    tmp = {}

    for i in inp[1:]:
        tmp = {}
        for j in feature_list:
            if j in i.keys():
                tmp[j] = i[j]
            else:
                tmp[j] = ""
        inp_process.append(tmp)

    ## feature extraction
    vec = DictVectorizer(sparse=False)
    X = vec.fit_transform(inp_process)

    ## impute the Nan that may occur

    ## feature selection

    ## clustering method
    ms = MeanShift(1.0)#estimate_bandwidth(X))
    label_list = ms.fit_predict(X)

    print label_list

#if __name__ == "__main__":
## have to define the input inp
#    user_identify(inp)
