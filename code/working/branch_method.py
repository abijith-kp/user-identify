import text_method
import dict_method
import try_new
import numpy as np
from sklearn.cluster import AffinityPropagation, MeanShift, estimate_bandwidth
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn import metrics
import nltk
import random
import difflib
import re


def cluster_full(inp):
    tv = TfidfVectorizer()
    cv = CountVectorizer()
    #X = tv.fit_transform(inp).toarray()
    #X = cv.fit_transform(inp).toarray()
    ap = AffinityPropagation(affinity="precomputed")
    #ms = MeanShift(estimate_bandwidth(X))

    N = len(inp)
    for i in xrange(N):
        inp[i] = inp[i].lower()
        #inp[i] = str.join(" ", inp[i])
    dist = np.zeros((N, N))
    for i in xrange(N):
        for j in xrange(N):
            dist[i][j] = difflib.SequenceMatcher(a=inp[i], b=inp[j]).ratio()
##            if dist[i][j] < 0.1:
##                dist[i][j] = 0

    lab = ap.fit_predict(dist)
    cc = ap.cluster_centers_indices_
    print lab, "====", cc
    count = 0
    for i in cc:
        print i
        count = 0
        for j in lab:
            if j == list(cc).index(i):
                print inp[count]
            count = count + 1
        print "--" * 10

def make_branch(term, email, inp_list):
    text_list = []
    correct_branch = []
    other_branch = []

    count = 0
    print term
    print "==" * 10
    if term == "email":
        text_list = [str.join(" ", i.values())    for pos, i in inp_list    ]
        for i in text_list:
            if i.find(email) >= 0:
                correct_branch.append(count)
            else:
                other_branch.append(count)
            count = count + 1
        return (correct_branch, other_branch)

    elif term.find("link") == 0:
        link_list = [i["link"]    for pos, i in inp_list    ]

        link_fb = re.compile(r'facebook\.com/([\w\._-]+)')
        link_gh = re.compile(r'github\.com/([\w\._-]+)')
        link_li1 = re.compile(r'linkedin.com/pub/([\w\._-]+)')
        link_li2 = re.compile(r'linkedin.com/in/([\w\._-]+)')
        link_tw = re.compile(r'twitter.com/([\w\._-]+)')
        link_pr = re.compile(r'pinrest.com/([\w\._-]+)')
        link_am = re.compile(r'aboutme.com/([\w\._-]+)')
        link_yt = re.compile(r'youtube.com/user/([\w\._-]+)')

        re_links = [link_fb, link_gh, link_li1, link_li2, link_tw, link_am, link_pr, link_yt]
        text_list = []
        for i in link_list:
            t = []
            for j in re_links:
                t.extend(j.findall(i))
            text_list.append(str.join(" ",list(set(t))))

        new_list = []
        for text in text_list:
            t = text.lower().split(" ")
            N = len(t)
            dist = np.zeros((N, N))
            tmp = []
            for x in xrange(N):
                for y in xrange(N):
                    dist[x][y] = difflib.SequenceMatcher(a=t[x], b=t[y]).ratio()
                    if dist[x][y] >= 0.8 and dist[x][y] != 1.0:
                        tmp.append(t[x])
                        tmp.append(t[y])
                    elif N == 1:
                        tmp.append(t[0])
            tmp = str.join(" ", list(set(tmp)))
            new_list.append(tmp)

        count = 0
        id_email = email.split("@")[0]
        for i in text_list:
            if i.find(id_email) >= 0:
                correct_branch.append(count)
            else:
                other_branch.append(count)
            count = count + 1
        return (correct_branch, other_branch)

    elif term.find("name") == 0:
        text_list = [i["name"]    for pos, i in inp_list    ]

    #print ">>>>>", text_list
    #cluster_full(text_list)
    return ([], [])

def find_user(email, inp_list):
    #imp = ["email"]
    imp = ["email", "link", "name"]
    #imp = ["email", 'name link', 'education', "description bio skills", "location"]
    l = inp_list[:]
    N = len(l)

    new_list = []
    count = 0

    text_list = []
    dict_cluster = {}
    correct_branch = []

    for count in xrange(len(imp)):
        new_list = []
        text_list = []
        k = imp[count]
        for pos, i in l:
            t = {}
            for j in i:
                for k_tmp in k.split(" "):
                    t.setdefault(k_tmp,"")
                    if j.find(k_tmp) >= 0 and i[j] != "":
                        if t.setdefault(k_tmp, i[j]) != None:
                            t[k_tmp] = t[k_tmp] + " " + i[j]
            new_list.append((pos, t))

        ## text_list = [str.join(" ", i.values()).lower()    for i in new_list    ]
        tmp_branch, other_branch = make_branch(k, email, new_list)
        for i in tmp_branch:
            l.pop(i)
        correct_branch.append(tmp_branch)
    print correct_branch
    x = []
    [x.extend(i)    for i in correct_branch    ]
    for i in x:
        print i, "==", inp_list[i][1]['profile_name']

def run(email):
    inp_list = try_new.new_input(try_new.X)
    find_user(email, zip(range(len(inp_list)),inp_list))

if __name__ == "__main__":
    email = "shankar@imsach.in"
    run(email)
