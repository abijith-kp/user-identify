l1 = text_method.create_input("sf", "../Results/atm/")
l2 = text_method.create_input("sf", "../Results/sriniketh/")
l3 = text_method.create_input("sf", "../Results/alexmathew/")

l = []
l.extend(l1)
l.extend(l2[1:])
l.extend(l2[1:])

l = dict_method.create_input(l)

tmp = []
tmp = [str.join(" ", i.values())    for i in l    ]

text_cat = []
[text_cat.append(str.join(" ", nltk.wordpunct_tokenize(i)))    for i in tmp    ]

cv = CountVectorizer()
X = cv.fit_transform(text_cat).toarray()
sim = metrics.pairwise.cosine_similarity(X)

ap = AffinityPropagation(affinity="precomputed")
print ap.fit_predict(sim)
