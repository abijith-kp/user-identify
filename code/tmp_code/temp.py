imp = ["location", "name", "skills", "education"]

cluster_list = []

cv = CountVectorizer(ngram_range=(1,4))
dv = DictVectorizer()
tv = TfidfVectorizer()
ap = AffinityPropagation(affinity="precomputed")
km = KMeans(n_clusters=4)
pca = PCA()
for name in imp:
    print "Using key: ", name
    x = []
    for i in l:
        t = {}
        for j in i.keys():
            if j.lower().find(name) >=0 :
                t[j] = i[j]
        x.append(t)

    X = dv.fit_transform(x).toarray()
    X = pca.fit_transform(X)
    sim = metrics.pairwise.cosine_similarity(X)
    print "\tDV ap:\t\t", ap.fit_predict(sim)
    print "\tDV km:\t\t", km.fit_predict(X)

    temp = {}
    for n in xrange(len(ap.cluster_centers_indices_)):
        temp[n] = []

    count = 0
    for n in ap.labels_:
        temp[n].append(count)
        count = count + 1

    print ap.cluster_centers_indices_
    cluster_list.append(temp)

    x = [str.join(" ", k.values())    for k in x    ]

    X = cv.fit_transform(x).toarray()
    X = pca.fit_transform(X)
    sim = metrics.pairwise.cosine_similarity(X)
    print "\tCV without nltk ap:", ap.fit_predict(sim)
    print "\tCV without nltk km:", km.fit_predict(X)

    X = tv.fit_transform(x).toarray()
    X = pca.fit_transform(X)
    sim = metrics.pairwise.cosine_similarity(X)
    print "\tTV without nltk ap:", ap.fit_predict(sim)
    print "\tTV without nltk km:", km.fit_predict(X)

    x = [str.join(" ", nltk.wordpunct_tokenize(k))    for k in x    ]

    X = cv.fit_transform(x).toarray()
    X = pca.fit_transform(X)
    sim = metrics.pairwise.cosine_similarity(X)
    print "\tCV with nltk ap:\t", ap.fit_predict(sim)
    print "\tCV with nltk km:", km.fit_predict(X)

    X = tv.fit_transform(x).toarray()
    X = pca.fit_transform(X)
    sim = metrics.pairwise.cosine_similarity(X)
    print "\tTV with nltk ap:\t", ap.fit_predict(sim)
    print "\tTV with nltk km:", km.fit_predict(X)
