for i in llt:
    t = []
    for j in i:
        if isinstance(j, dict):
            p = [x    for x in j.values()    if x != None]
            t.extend([str(x)    for x in p    if isinstance(x, int)])
        elif isinstance(j, unicode):
            t.append(j)
    lllt.append(t)
