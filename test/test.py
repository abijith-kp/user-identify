import json
import os

root_path = "../Results"
## lDict = dict()

def convert(username):
        global root_path
        
        lDict = dict()
        
        for name in os.listdir(root_path):
                split_name = name.split(".")
                if split_name[0] != "" and split_name[-1] == "json":
                        temp_json = json.loads(open(root_path + "/" + name, 'r').read())
                        lDict[(username, split_name[0])] = temp_json
                        
        return lDict

if __name__ == "__main__":
        lDict = convert("test")
        
        print lDict